<?php


namespace TestCompany\Components\Dispatcher;


use TestCompany\Components\Dispatcher\Event\EventInterface;

interface DispatcherInterface
{
    public function dispatch(EventInterface $event);
}