<?php


namespace TestCompany\Components\Dispatcher\Event;


interface EventInterface
{
    public function getName() : string;
}