<?php


namespace TestCompany\Components\CommandBus;


use TestCompany\Components\CommandBus\Command\CommandInterface;
use TestCompany\Components\CommandBus\CommandBag\CommandBagInterface;
use TestCompany\Components\CommandBus\Exception\CommandBusException;
use TestCompany\Components\Validator\ValidatorInterface;

/**
 * Class CommandBus
 * @package TestCompany\Components\CommandBus
 */
class CommandBus implements CommandBusInterface
{
    /**
     * @var CommandBagInterface $commandBag
     */
    protected $commandBag;

    /**
     * @var ValidatorInterface $validator
     */
    protected $validator;

    /**
     * CommandBus constructor.
     * @param ValidatorInterface|null $validator
     * @param CommandBagInterface|null $commandBag
     */
    public function __construct(
        ValidatorInterface $validator = null,
        CommandBagInterface $commandBag = null
    )
    {
        $this->commandBag = $commandBag;
        $this->validator = $validator;
    }

    /**
     * @param string $commandClassName
     * @param $arguments
     * @throws CommandBusException
     */
    public function apply(string $commandClassName, $arguments)
    {
        if(!$this->commandBag->exist($commandClassName)){
            throw new CommandBusException(sprintf("Can't find command with the given name [%s].", $commandClassName));
        }

        /** @var CommandInterface $command */
        $command = $this->commandBag->get($commandClassName);

        if($command->getValidation()){
            $this->validator->validate($arguments, $command->getValidation());
        }

        $commandResult = $command->exec($arguments);

        return $commandResult;
    }

    /**
     * @param CommandInterface $command
     */
    public function registerCommand(CommandInterface $command)
    {
        $this->commandBag->add(get_class($command), $command);
    }
}