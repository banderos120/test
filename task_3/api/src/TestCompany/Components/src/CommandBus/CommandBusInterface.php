<?php


namespace TestCompany\Components\CommandBus;


use TestCompany\Components\CommandBus\Command\CommandInterface;

interface CommandBusInterface
{
    public function apply(string $commandClassName, $arguments);

    public function registerCommand(CommandInterface $command);
}