<?php


namespace TestCompany\Components\CommandBus\CommandBag;


use TestCompany\Components\CommandBus\Command\CommandInterface;

interface CommandBagInterface extends \Iterator
{
    public function get($commandName);

    public function exist($commandName);

    public function add($commandName, CommandInterface $command);
}