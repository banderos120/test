<?php


namespace TestCompany\Components\CommandBus\Result;

/**
 * Class CommandResult
 * @package TestCompany\Components\CommandBus\Result
 */
class CommandResult
{
    /**
     * @var mixed
     */
    protected $commandResult;

    /**
     * CommandResult constructor.
     * @param $commandResult
     */
    public function __construct($commandResult)
    {
        $this->commandResult = $commandResult;
    }

    /**
     * @return mixed
     */
    public function getCommandResult()
    {
        return $this->commandResult;
    }
}