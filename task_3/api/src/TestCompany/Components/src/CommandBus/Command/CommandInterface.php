<?php


namespace TestCompany\Components\CommandBus\Command;


interface CommandInterface
{
    public function exec($arguments);

    public function getValidation();
}