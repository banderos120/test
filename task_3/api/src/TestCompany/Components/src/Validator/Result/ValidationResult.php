<?php


namespace TestCompany\Components\Validator\Result;

/**
 * Class ValidationResult
 * @package TestCompany\Components\Validator\Result
 */
class ValidationResult
{
    /**
     * @var bool
     */
    protected $isSuccess;

    /**
     * @var array
     */
    protected $properties;

    /**
     * @var string
     */
    protected $error;

    /**
     * ValidationResult constructor.
     * @param bool $isSuccess
     * @param array $properties
     */
    public function __construct(
        bool $isSuccess = true,
        array $properties = []
    )
    {
        $this->isSuccess = $isSuccess;
        $this->properties = $properties;
    }

    /**
     * @param bool $success
     * @return bool
     */
    public function isSuccess($success = true) : bool
    {
        return $this->isSuccess;
    }

    /**
     * @param string $error
     */
    public function setError(string $error)
    {
        $this->error = $error;
    }

    /**
     * @return string
     */
    public function getError(): string
    {
        return $this->error;
    }

    /**
     * @param string $parameter
     * @param $error
     */
    public function addError(string $parameter, $error)
    {
        if(!isset($this->properties[$parameter])){
            $this->properties[$parameter] = [];
        }

        $this->properties[$parameter][] = $error;
    }

    /**
     * @return array
     */
    public function getErrors()
    {
        return $this->properties;
    }
}