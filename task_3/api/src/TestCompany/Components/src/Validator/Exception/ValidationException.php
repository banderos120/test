<?php


namespace TestCompany\Components\Validator\Exception;


use TestCompany\Components\Validator\Result\ValidationResult;
use Throwable;

/**
 * Class ValidationException
 * @package TestCompany\Components\Validator\Exception
 */
class ValidationException extends ValidatorException
{
    /**
     * @var ValidationResult
     */
    protected $validationResult;

    /**
     * ValidationException constructor.
     * @param ValidationResult $validationResult
     * @param string $message
     * @param int $code
     * @param Throwable|null $previous
     */
    public function __construct(ValidationResult $validationResult, $message = "", $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);

        $this->validationResult = $validationResult;
    }

    public function getValidationResult() : ValidationResult
    {
        return $this->validationResult;
    }
}