<?php


namespace TestCompany\Components\Validator;


use TestCompany\Components\Validator\Rule\ValidationRuleInterface;

interface ValidatorInterface
{
    public function validate($data, ValidationRuleInterface $validationRule);
}