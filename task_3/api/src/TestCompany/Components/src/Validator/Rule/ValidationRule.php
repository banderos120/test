<?php


namespace TestCompany\Components\Validator\Rule;

/**
 * Class ValidationRule
 * @package TestCompany\Components\Validator\Rule
 */
class ValidationRule implements ValidationRuleInterface
{
    /**
     * @var mixed
     */
    protected $rules;

    /**
     * ValidationRule constructor.
     * @param $rules
     */
    public function __construct($rules)
    {
        $this->rules = $rules;
    }

    /**
     * @return mixed
     */
    public function getRules()
    {
        return $this->rules;
    }
}