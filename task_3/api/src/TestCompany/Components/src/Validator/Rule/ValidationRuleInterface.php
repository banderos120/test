<?php


namespace TestCompany\Components\Validator\Rule;


interface ValidationRuleInterface
{
    public function getRules();
}