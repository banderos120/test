<?php


namespace TestCompany\Components\Validator;


use TestCompany\Components\Validator\Exception\ValidationException;
use TestCompany\Components\Validator\Exception\ValidatorException;
use TestCompany\Components\Validator\Result\ValidationResult;
use TestCompany\Components\Validator\Rule\ValidationRuleInterface;

class ClosureValidator implements ValidatorInterface
{

    public function validate($data, ValidationRuleInterface $validationRule)
    {
        $closure = $validationRule->getRules();

        if(is_callable($closure)){
            throw new ValidatorException("Closure validator requires the callback validation rule.");
        }

        $validationResult = $closure($data);

        if(!$validationResult instanceof ValidationResult){
            throw new ValidatorException("The validation rule of the command must returns the ValidationResult class.");
        }

        if(!$validationResult->isSuccess()){
            throw new ValidationException($validationResult);
        }
    }
}