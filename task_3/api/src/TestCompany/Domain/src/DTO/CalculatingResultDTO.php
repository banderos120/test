<?php


namespace TestCompany\Domain\DTO;


class CalculatingResultDTO
{
    protected $paymentNumber;

    protected $paymentDate;

    protected $mainDebt;

    protected $percents;

    protected $totalAmount;

    protected $balanceOwed;
}