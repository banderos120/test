<?php


namespace TestCompany\Domain\Exception;


class DomainException extends \Exception
{
    const CODE_BAD_REQUEST = 400;
    const CODE_NOT_FOUND = 404;
    const CODE_ACCESS_DENIED = 401;
}