<?php


namespace TestCompany\Domain\Repository;


use Doctrine\ORM\EntityManagerInterface;

class CreditCalculatorRepository
{
    protected $em;

    public function __construct(
        EntityManagerInterface $em
    )
    {
        $this->em = $em;
    }
}