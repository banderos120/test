<?php


namespace TestCompany\Domain\Commands\Query;


use TestCompany\Components\CommandBus\Command\CommandInterface;
use TestCompany\Components\Validator\Result\ValidationResult;
use TestCompany\Components\Validator\Rule\ValidationRule;
use TestCompany\CreditCalculator\CalculationResultInterface;
use TestCompany\CreditCalculator\Condition\AnnuityCreditCondition;
use TestCompany\CreditCalculator\CreditCalculatorInterface;

/**
 * Class Calculate
 * @package TestCompany\Domain\Commands\Statement
 */
class Calculate implements CommandInterface
{
    /**
     * @var CreditCalculatorInterface
     */
    protected $creditCalculator;

    /**
     * Calculate constructor.
     * @param CreditCalculatorInterface $creditCalculator
     */
    public function __construct(
        CreditCalculatorInterface $creditCalculator
    )
    {
        $this->creditCalculator = $creditCalculator;
    }

    /**
     * @param $arguments
     * @return CalculationResultInterface
     */
    public function exec($arguments)
    {
        /** @var CalculationResultInterface $calculationResult */
        $calculationResult = $this->creditCalculator->calculate(new AnnuityCreditCondition(
            $arguments['creditAmount'],
            new \DateInterval(sprintf('M%d', $arguments['creditMonthInterval'])),
            $arguments['rate'],
            new \DateTime($arguments['firstPaymentDay'])
        ));

        return $calculationResult;
    }

    /**
     * @return ValidationRule
     */
    public function getValidation()
    {
        return new ValidationRule(function($data){
            $result = new ValidationResult();

            if(!isset($data['creditAmount'], $data['creditMonthInterval'], $data['rate'], $data['firstPaymentDay'])){
                $result->isSuccess(false);
                $result->setError('Wrong parameters.');
            }

            return $result;
        });
    }
}