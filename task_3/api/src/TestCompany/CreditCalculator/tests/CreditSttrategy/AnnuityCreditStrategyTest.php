<?php


namespace TestCompany\Tests\CreditStrategy;


use PHPUnit\Framework\TestCase;
use TestCompany\CreditCalculator\CalculationResultInterface;
use TestCompany\CreditCalculator\Condition\AnnuityCreditCondition;
use TestCompany\CreditCalculator\CreditStrategy\AnnuityCreditStrategy;

class AnnuityCreditStrategyTest extends TestCase
{
    public function testCalculate()
    {
        $period = 12*20;

        $creditCondition = new AnnuityCreditCondition(
            100000,
            new \DateInterval("P{$period}M"),
            10,
            new \DateTime('2010-01-01 00:00:00')
        );

        $creditStrategy = new AnnuityCreditStrategy();

        $calculationResult = $creditStrategy->calculate($creditCondition);

        $this->assertInstanceOf(CalculationResultInterface::class, $calculationResult);
    }
}