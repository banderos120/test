<?php


namespace TestCompany\CreditCalculator;


use TestCompany\CreditCalculator\Condition\CreditCondition;
use TestCompany\CreditCalculator\ConditionHandler\CreditStrategyInterface;

interface CreditCalculatorInterface
{
    public function calculate(CreditCondition $condition);

    public function registerStrategy(CreditStrategyInterface $creditStrategy);
}