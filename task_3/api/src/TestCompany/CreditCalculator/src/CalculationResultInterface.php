<?php


namespace TestCompany\CreditCalculator;



use TestCompany\CreditCalculator\Calculation\Calculation;

interface CalculationResultInterface extends \Iterator
{
    public function addCalculation(Calculation $calculationResult);

    public function getByNumber(int $number);

    public function getByMonth(int $month);
}