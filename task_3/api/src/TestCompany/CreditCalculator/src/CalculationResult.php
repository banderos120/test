<?php


namespace TestCompany\CreditCalculator;

use TestCompany\CreditCalculator\Calculation\Calculation;

/**
 * Class AbstractCalculation
 * @package TestCompany\CreditCalculator\CalculationResult
 */
class CalculationResult implements CalculationResultInterface
{
    /**
     * @var Calculation[]
     */
    protected $calculations = [];

    /**
     * @param int $number
     * @return Calculation
     */
    public function getByNumber(int $number)
    {
        return $this->calculations[$number] ?? null;
    }

    /**
     * @param int $month
     * @return Calculation
     */
    public function getByMonth(int $month)
    {
        foreach ($this->calculations as $calculation){
            if($calculation->getDate()->format('m') == $month){
                return $calculation;
            }
        }
    }

    /**
     * @param Calculation $calculationResult
     */
    public function addCalculation(Calculation $calculationResult)
    {
        $this->calculations[] = $calculationResult;
    }

    /**
     * @return mixed|Calculation
     */
    public function current()
    {
        return current($this->calculations);
    }

    /**
     *
     */
    public function next()
    {
        next($this->calculations);
    }

    /**
     * @return int|null|string
     */
    public function key()
    {
        return key($this->calculations);
    }

    /**
     * @return bool
     */
    public function valid()
    {
        return $this->current() !== false;
    }

    /**
     *
     */
    public function rewind()
    {
        reset($this->calculations);
    }

}