<?php


namespace TestCompany\CreditCalculator\Condition;


class CreditCondition
{
    /**
     * @var double
     */
    protected $amount;

    /**
     * @var \DateInterval
     */
    protected $timeDuration;

    /**
     * @var int
     */
    protected $rate;

    /**
     * @var \DateTime
     */
    protected $startPaymentDate;

    /**
     * CreditCondition constructor.
     * @param float $amount
     * @param \DateInterval $timeDuration
     * @param int $rate
     * @param \DateTime $startPaymentDate
     */
    public function __construct($amount, \DateInterval $timeDuration, $rate, \DateTime $startPaymentDate)
    {
        $this->amount = $amount;
        $this->timeDuration = $timeDuration;
        $this->rate = $rate;
        $this->startPaymentDate = $startPaymentDate;
    }

    /**
     * @return float
     */
    public function getAmount(): float
    {
        return $this->amount;
    }

    /**
     * @param float $amount
     */
    public function setAmount(float $amount)
    {
        $this->amount = $amount;
    }

    /**
     * @return \DateInterval
     */
    public function getTimeDuration(): \DateInterval
    {
        return $this->timeDuration;
    }

    /**
     * @param \DateInterval $timeDuration
     */
    public function setTimeDuration(\DateInterval $timeDuration)
    {
        $this->timeDuration = $timeDuration;
    }

    /**
     * @return int
     */
    public function getRate(): int
    {
        return $this->rate;
    }

    /**
     * @param int $rate
     */
    public function setRate(int $rate)
    {
        $this->rate = $rate;
    }

    /**
     * @return \DateTime
     */
    public function getStartPaymentDate(): \DateTime
    {
        return $this->startPaymentDate;
    }

    /**
     * @param \DateTime $startPaymentDate
     */
    public function setStartPaymentDate(\DateTime $startPaymentDate)
    {
        $this->startPaymentDate = $startPaymentDate;
    }
}