<?php


namespace TestCompany\CreditCalculator\Calculation;


class Calculation
{
    /**
     * @var \DateTime
     */
    protected $date;

    /**
     * @var float
     */
    protected $cashFlow;

    /**
     * @var float
     */
    protected $repaymentPercents;

    /**
     * @var float
     */
    protected $mainDebt;

    /**
     * @var float
     */
    protected $balanceOfPrincipal;

    /**
     * CalculationResult constructor.
     * @param \DateTime $date
     * @param float $cashFlow
     * @param float $repaymentPercents
     * @param float $mainDebt
     * @param float $balanceOfPrincipal
     */
    public function __construct(\DateTime $date, $cashFlow, $repaymentPercents, $mainDebt, $balanceOfPrincipal)
    {
        $this->date = $date;
        $this->cashFlow = $cashFlow;
        $this->repaymentPercents = $repaymentPercents;
        $this->mainDebt = $mainDebt;
        $this->balanceOfPrincipal = $balanceOfPrincipal;
    }

    /**
     * @return \DateTime
     */
    public function getDate(): \DateTime
    {
        return $this->date;
    }

    /**
     * @return float
     */
    public function getCashFlow(): float
    {
        return $this->cashFlow;
    }

    /**
     * @return float
     */
    public function getRepaymentPercents(): float
    {
        return $this->repaymentPercents;
    }

    /**
     * @return float
     */
    public function getMainDebt(): float
    {
        return $this->mainDebt;
    }

    /**
     * @return float
     */
    public function getBalanceOfPrincipal(): float
    {
        return $this->balanceOfPrincipal;
    }
}