<?php


namespace TestCompany\CreditCalculator\CreditStrategy;


use TestCompany\CreditCalculator\CalculationResultInterface;
use TestCompany\CreditCalculator\Condition\CreditCondition;

interface CreditStrategyInterface
{
    public function calculate(CreditCondition $condition) : CalculationResultInterface;

    public function getSupportedCondition() : string ;
}