<?php


namespace TestCompany\CreditCalculator\CreditStrategy;


use TestCompany\CreditCalculator\Calculation\Calculation;
use TestCompany\CreditCalculator\CalculationResult;
use TestCompany\CreditCalculator\CalculationResultInterface;
use TestCompany\CreditCalculator\Condition\AnnuityCreditCondition;
use TestCompany\CreditCalculator\Condition\CreditCondition;

/**
 * Class AnnuityCreditStrategy
 * @package TestCompany\CreditCalculator\ConditionHandler
 */
class AnnuityCreditStrategy implements CreditStrategyInterface
{
    /**
     * @param CreditCondition $condition
     * @return CalculationResultInterface
     */
    public function calculate(CreditCondition $condition) : CalculationResultInterface
    {
        /** @var \DateInterval $month */
        $interval = $condition->getTimeDuration();

//        $monthPayment = ($condition->getAmount() * ($condition->getRate() / 100)) / (1 - pow((1 + ($condition->getRate() / 100)), -$interval->m));
        $tmp = pow( 1 + $condition->getRate() / 100, 1/12);
        $monthPayment = round(($condition->getAmount() * pow( $tmp, $interval->m) * ($tmp - 1)) / (pow($tmp, $interval->m) - 1), 2);

        /** @var \DateTime $startPaymentDate */
        $startPaymentDate = $condition->getStartPaymentDate();

        $calculationResult = new CalculationResult();

        $balanceOfPrincipal = $condition->getAmount();

        $daysCount = $startPaymentDate->format('t');

        for($i = 1; $i <= $interval->m; $i++){

            $startPaymentDate->modify('+1 month');

            $percents = round((pow(1.1, $daysCount / 365) - 1) * $balanceOfPrincipal, 2);

            $mainDebt = round($monthPayment - $percents, 2);

            $balanceOfPrincipal = round($balanceOfPrincipal - $mainDebt, 2);

            $calculationResult->addCalculation(new Calculation(
                clone $startPaymentDate,
                $monthPayment,
                $percents,
                $mainDebt,
                $balanceOfPrincipal
            ));

            $daysCount = $startPaymentDate->format('t');

        }

        return $calculationResult;
    }

    /**
     * @return string
     */
    public function getSupportedCondition(): string
    {
        return AnnuityCreditCondition::class;
    }
}