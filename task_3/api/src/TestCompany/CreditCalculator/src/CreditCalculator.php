<?php


namespace TestCompany\CreditCalculator;


use TestCompany\CreditCalculator\Condition\CreditCondition;
use TestCompany\CreditCalculator\ConditionHandler\CreditStrategyInterface;
use TestCompany\CreditCalculator\Exception\CreditCalculatorException;

/**
 * Class CreditCalculator
 * @package TestCompany\CreditCalculator
 */
class CreditCalculator implements CreditCalculatorInterface
{
    /**
     * @var CreditStrategyInterface[] $strategies
     */
    protected $strategies;

    /**
     * @param CreditCondition $condition
     * @return mixed
     * @throws CreditCalculatorException
     */
    public function calculate(CreditCondition $condition)
    {
        $conditionName = get_class($condition);

        if(!isset($this->strategies[$conditionName])){
            throw new CreditCalculatorException(sprintf("There is no credit handler for the given condition name %s.", $conditionName));
        }

        /** @var CreditStrategyInterface $creditStrategy */
        $creditStrategy = $this->strategies[$conditionName];

        $calculation = $creditStrategy->calculate($condition);

        return $calculationResult;
    }

    /**
     * @param CreditStrategyInterface $creditStrategy
     * @throws CreditCalculatorException
     */
    public function registerStrategy(CreditStrategyInterface $creditStrategy)
    {
        $conditionName = $creditStrategy->getSupportedCondition();

        if(isset($this->strategies[$conditionName])){

            $existedCalculator = get_class($this->strategies[$conditionName]);

            throw new CreditCalculatorException(sprintf("There is already exists calculator [%s] for this condition [%s].", $existedCalculator, $conditionName));
        }

        $this->strategies[$creditStrategy->getSupportedCondition()] = $creditStrategy;
    }
}