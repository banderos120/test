<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use TestCompany\Components\CommandBus\CommandBus;
use TestCompany\Components\Validator\Exception\ValidationException;
use TestCompany\Domain\Commands\Query\Calculate;

class CreditCalculatorController extends Controller
{
    /**
     * @param Request $request
     *
     * @Route()
     */
    public function calculateAction(Request $request)
    {
        /** @var CommandBus $commandBus */
        $commandBus = $this->get('test_company.command_bus');

        try{
            $commandBus->apply(Calculate::class, $request->getContent());
        }catch (ValidationException $exception){
            throw new BadRequestHttpException($exception->getValidationResult()->getError());
        }
    }
}
