<?php

$targetName = './target.txt';
$sourceName = './text.txt';

$file = fopen($sourceName, 'r');
$target = fopen($targetName, 'w+');

$readLength = 10512;
$wordStart = false;
$startPosition = 0;

$fileSize = filesize($sourceName);
$position = -1;
$chars = [];
$values = [3 => 'ТРИ',5 => 'ПЯТЬ',15 => 'ПЯТНАДЦАТЬ'];

for($readed = 1; $readed <= $fileSize; $readed += $readLength){

    if($readed < $fileSize && $readed + $readLength > $fileSize){
        $readLength = $fileSize;
    }

    $inBuffer = preg_split('//u',fread($file, $readLength) ?? '', -1);
    $buffer = '';
    $bufferLength = count($inBuffer);

    for ($i = 0; $i < $bufferLength ; $i++){
        $position++;
        $char = $inBuffer[$i];

        $asciiDec = $chars[$char] ?? $chars[$char] = ord($char);

        if(!(($asciiDec < 48) || ($asciiDec > 57 && $asciiDec < 65) || ($asciiDec > 90 && $asciiDec < 97) || ( $asciiDec > 122  && $asciiDec <= 127) || ($asciiDec > 200 && $asciiDec < 300))){

            if(!$wordStart){
                $wordStart = true;
                $startPosition = $position;
                $replaceInProgress = false;

                foreach ($values as $int => $word){
                    if($startPosition % $int === 0){
                        $char = $word;
                        $replaceInProgress = true;
                    }
                }
            }elseif($replaceInProgress) continue;

        }else{
            $wordStart = false;
            $replace = false;
        }

        $buffer .= $char;

    }

    fwrite($target, $buffer);

}

fclose($target);
fclose($file);
